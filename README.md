# ec2-bootstrap project
The ultimate goal of this project is to automate the process of "bootstrapping" AWS EC2 instances using a dynamic inventory to identify the servers and ansible to create an ansible, admin and non-privileged user replace the ec2-user keys and set the hostnames based on a tag read from the inventory.

## Playbooks
ping.yml - refreshes the inventory and then pings the servers to validate ansible access

bootstrap.yml - main playbook

keys.yml  - replaces the ec2-user key used to access the newly built system and adds admins keys to the ansible user authorized_keys.

users.yml - creates admin user(s) and adds keys to allow access.

hostname.yml - changes the server hostname

## Other things to know
Execute the playbook using the temporary instance creation keys:

```ansible-playbook playbooks/ping.yml --extra-vars="ansible_ssh_private_key_file=/Users/drode/git/ansible/bootstrap/playbooks/files/AWS2-newInstances.pem"```


Dynamic aws inventory via ec2.py script (hosts)  https://docs.ansible.com/ansible/2.6/user_guide/intro_dynamic_inventory.html#example-aws-ec2-external-inventory-script


ansible.cfg in execution dir above playbook dir which identifies path the playbook/hosts. Modify this to point to the location of the hosts file (ec2.py)


~/.aws/credentials - This is how the ec2.py script accesses the AWS account from which the inventory is generated. Update this or update the ec2.py config to directly specify AWS credentials.


MacOS automatically caches any keys used, so use ssh-add -D to clear keys before running a playbook that relies on loading a specific key.

## References
http://minimum-viable-automation.com/ansible/use-ansible-create-user-accounts-setup-ssh-keys/
